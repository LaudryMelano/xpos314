﻿        using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xpos314.viewmodels
{
    public class VMTblVariant
    {
        [Key]
        public int Id { get; set; }
        public int IdCategory { get; set; }
        [StringLength(50)]
    
        public string NameVariant { get; set; } = null!;
        [StringLength(100)]
        public string NameCategory { get; set; } = null!;
      
        public string? Description { get; set; }
        public bool? IsDelete { get; set; }
        public int CreateBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
