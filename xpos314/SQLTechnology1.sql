CREATE DATABASE XPOS_314

CREATE TABLE TblCategory(
Id int primary key identity(1,1)  not null,
NameCategory varchar(50) null,
Description varchar(100) null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

SELECT * FROM TblCategory