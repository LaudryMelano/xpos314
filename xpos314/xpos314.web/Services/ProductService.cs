﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class ProductService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public ProductService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMTblProduct>> GetAllData()
        {
            List<VMTblProduct> data = new List<VMTblProduct>();
            string apiRespone = await client.GetStringAsync(RouteAPI + "apiProduct/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblProduct>>(apiRespone)!;
            return data;
        }

        public async Task<VMResponse> Create(VMTblProduct dataParam)
        {
            //Proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //Proses mengubah string menjadi json lalu dikirim sebagai Request Body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //Proses memanggil API dan mengirimkan Body
            var request = await client.PostAsync(RouteAPI + "apiProduct/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
            }
            return response;
        }

        public async Task<VMTblProduct> GetDataById(int id)
        {
            VMTblProduct data = new VMTblProduct();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduct/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblProduct>(apiResponse)!;
            return data;
        }

        public async Task<VMResponse> Edit(VMTblProduct dataParam)
        {
            //Proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //Proses mengubah string menjadi json lalu dikirim sebagai Request Body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //Proses memanggil API dan mengirimkan Body
            var request = await client.PutAsync(RouteAPI + "apiProduct/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
            }
            return response;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiProduct/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
            }
            return response;
        }
    }
}
