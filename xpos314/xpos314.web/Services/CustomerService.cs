﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class CustomerService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public CustomerService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMTblCustomer>> GetAllData()
        {
            List<VMTblCustomer> data = new List<VMTblCustomer>();
            string apiRespone = await client.GetStringAsync(RouteAPI + "apiCustomer/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblCustomer>>(apiRespone)!;
            return data;
        }
        public async Task<VMResponse> Create(VMTblCustomer dataParam)
        {
            //Proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //Proses mengubah string menjadi json lalu dikirim sebagai Request Body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //Proses memanggil API dan mengirimkan Body
            var request = await client.PostAsync(RouteAPI + "apiCustomer/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
            }
            return response;
        }

        public async Task<VMTblCustomer> GetDataById(int id)
        {
            VMTblCustomer data = new VMTblCustomer();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiResponse)!;
            return data;
        }

        public async Task<VMResponse> Edit(VMTblCustomer dataParam)
        {
            //Proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //Proses mengubah string menjadi json lalu dikirim sebagai Request Body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan Body
            var request = await client.PutAsync(RouteAPI + "apiCustomer/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
            }
            return response;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCustomer/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
            }
            return response;
        }
    }
}
