﻿using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;
using Newtonsoft.Json;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class CategoryService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public CategoryService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblCategory>> GetAllData()
        {
            List<TblCategory> data = new List<TblCategory>();
            string apiResponse = await client.GetStringAsync(RouteAPI+"apiCategory/GetAllData");

            data = JsonConvert.DeserializeObject<List<TblCategory>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(TblCategory dataParam)
        {
            //Proses convert dari obj to string
            string json = JsonConvert.SerializeObject(dataParam);


            //Proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memangil API dan mengirim data body
            var request = await client.PostAsync(RouteAPI + "apiCategory/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<bool> CheckCategoryByName(string nameCategory)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/CheckCategoryByName/{nameCategory}" );

            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);

            return isExist;
        }

        public async Task<TblCategory> GetDataById(int id)
        {
            TblCategory data = new TblCategory();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblCategory>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(TblCategory dataParam)
        {
            //Proses convert dari obj to string
            string json = JsonConvert.SerializeObject(dataParam);


            //Proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memangil API dan mengirim data body
            var request = await client.PutAsync(RouteAPI + "apiCategory/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCategory/Delete/{id}/{createBy}");

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;

        }

    }
}
