﻿using Newtonsoft.Json;
using System.Drawing;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class AuthService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public AuthService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<VMTblCustomer> CheckLogin(string email, string password)
        {
            VMTblCustomer data = new VMTblCustomer();
            string apiRespone = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckLogin/{email}/{password}");
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiRespone)!;
            return data;
        }

        public async Task<List<VMMenuAccess>> MenuAccess(int IdRole)
        {
            List<VMMenuAccess> data = new List<VMMenuAccess>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/MenuAccess/{IdRole}");
            data = JsonConvert.DeserializeObject<List<VMMenuAccess>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> CreateCustomer(TblCustomer dataParam)
        {
            //Proses convert dari obj to string
            string json = JsonConvert.SerializeObject(dataParam);


            //Proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memangil API dan mengirim data body
            var request = await client.PostAsync(RouteAPI + "apiAuth/CreateCustomer", content);

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke object
                response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }

        public async Task<bool> CheckEmailExisting(string email)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckEmailExisting/{email}");

            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);

            return isExist;
        }

    }
}
