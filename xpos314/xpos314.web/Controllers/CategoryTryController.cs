﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CategoryTryController : Controller
    {
        private readonly XPOS_314Context db;
        private readonly CategoryTryService categoryTryService;

        public CategoryTryController(XPOS_314Context _db)
        {
            this.db = _db;
            this.categoryTryService = new CategoryTryService(db);
        }

        public IActionResult Index()
        {
            List<VMTblCategory> dataView = categoryTryService.GetAllData();
            return View(dataView);
        }

        public IActionResult Create() 
        {
            VMTblCategory viewModel = new VMTblCategory();
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Create(VMTblCategory dataView)
        {
            VMResponse respon = new VMResponse();

            if(ModelState.IsValid)
            {
                respon = categoryTryService.Create(dataView);

                if (respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Edit(int id)
        {
            VMTblCategory dataView = categoryTryService.GetById(id);
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Edit(VMTblCategory dataView)
        {
            VMResponse respon = new VMResponse();

            if(ModelState.IsValid)
            {
                respon = categoryTryService.Edit(dataView);
                if (respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Detail(int id)
        {
            VMTblCategory dataView = categoryTryService.GetById(id);
            return View(dataView);
        }

 
        public IActionResult Delete(int id)
        {
            VMTblCategory dataView = categoryTryService.GetById(id);
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Delete(VMTblCategory dataView)
        {

            /*Menghapus menggunakan validasi*/
            /*VMResponse respon = new VMResponse();
            //Menghapus kolom yang required saat model divalidasi//
            ModelState.Remove("NameCategory");
            if (ModelState.IsValid)
            {
                respon = categoryTryService.Delete(dataView);
                if (respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }*/

            VMResponse respon = categoryTryService.Delete(dataView);
            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            respon.Entity = dataView;
            return View(respon.Entity);
        }
    }
}
