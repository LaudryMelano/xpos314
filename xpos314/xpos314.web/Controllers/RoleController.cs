﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class RoleController : Controller
    {
        private readonly RoleService roleService;
        public IConfiguration configuration;
        public int IdUser = 1;
        public RoleController(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.roleService = new RoleService(this.configuration);
        }
        public async Task<IActionResult> Index(
            string sortOrder,
            string searchString,
            string currentFilter,
            int? pageNumber,
            int? pageSize
            )
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<TblRole> data = await roleService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.RoleName.ToLower().Contains(searchString.ToLower())
                || a.RoleName.ToLower().Contains(searchString.ToLower())
                ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
            }

            return View(PaginatedList<TblRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            TblRole data = new TblRole();
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(TblRole dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await roleService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string nameRole)
        {
            bool isExist = await roleService.CheckRoleByName(nameRole);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblRole dataParam)
        {
            dataParam.UpdatedBy = IdUser;
            VMResponse respons = await roleService.Edit(dataParam);

            if (respons.Success)
            {
                return Json(new { dataRespon = respons });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int createdBy = IdUser;
            VMResponse respon = await roleService.Delete(id, createdBy);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", id);
            }
        }
    }
}
