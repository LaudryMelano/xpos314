﻿using Microsoft.AspNetCore.Mvc;
using xpos314.web.Models;

namespace xpos314.web.Controllers
{
    public class FriendController : Controller
    {
        private static List<Friend> lst = new List<Friend>()
            {
                new Friend(){Id = 1, Name ="Anwar", Address="Ranggunan"},
                new Friend(){Id = 2, Name ="Asti", Address="Garut"},
            };
        public IActionResult Index()
        {
            //Friend friend = new Friend();
            //friend.Id = 3;
            //friend.Name = "Isni";
            //friend.Address = "Cimahi";

            //lst.Add(friend);

            ViewBag.lstFriend = lst;

            return View(lst);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Friend friend)
        {
            lst.Add(friend);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id) 
        {
            Friend friend = lst.Find(a=>a.Id == id)!;
            return View(friend);
        }

        [HttpPost]
        public IActionResult Edit(Friend data)
        {
            Friend friend = lst.Find(b=>b.Id == data.Id)!;
            int index = lst.IndexOf(friend);

            if(index > -1)
            {
                lst[index].Id = data.Id;
                lst[index].Name = data.Name;
                lst[index].Address = data.Address;
            }
            return RedirectToAction("Index");
        }

        public IActionResult Detail(int id)
        {
            Friend friend = lst.Find(a => a.Id == id)!;
            return View(friend);
        }

        //public IActionResult Delete(int id)
        //{
        //    Friend friend = lst.Find(a => a.Id == id)!;
        //    return View(friend);
        //}

        //[HttpPost]
        //public IActionResult Delete(string id)
        //{
        //    Friend friend = lst.Find(a => a.Id == int.Parse(id))!;
        //    lst.RemoveAt(friend.Id);
        //    return RedirectToAction("Index");
        //}

        [HttpGet]
        [HttpPost]
        public IActionResult Delete(int id)
        {
            Friend friend = lst.Find(a=> a.Id == id)!;
            if(HttpContext.Request.Method == "POST")
            {
                lst.Remove(friend);
                return RedirectToAction("Index");
            }
            else
            {
                return View(friend);
            }
        }

    }
}
