﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CustomerController : Controller
    {
        private RoleService roleService;
        private CustomerService customerService;
        private int IdUser = 1;

        public CustomerController(RoleService _roleService, CustomerService _customerService)
        {
            this.roleService = _roleService;
            this.customerService = _customerService;
        }

        public async Task<IActionResult> Index(string sortOrder,
                                                string searchString,
                                                string currentFilter,
                                                int? pageNumber,
                                                int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            List<VMTblCustomer> data = await customerService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameCustomer.ToLower().Contains(searchString.ToLower())
                || a.Email.ToLower().Contains(searchString.ToLower())
                || a.Address.ToLower().Contains(searchString.ToLower())
                || a.RoleName.ToLower().Contains(searchString.ToLower())
                || a.Phone.ToLower().Contains(searchString.ToLower())
                 ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameCustomer).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameCustomer).ToList();
                    break;
            }
            return View(PaginatedList<VMTblCustomer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Create()
        {
            VMTblCustomer data = new VMTblCustomer();
            List<TblRole> listRole = await roleService.GetAllData();
            ViewBag.ListRole = listRole;
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMTblCustomer dataParam)
        {
            VMResponse respon = await customerService.Create(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            List<TblRole> listRole = await roleService.GetAllData();
            ViewBag.ListRole = listRole;
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMTblCustomer dataParam)
        {
            VMResponse respon = await customerService.Edit(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await customerService.Delete(id);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }
            else
                return RedirectToAction("Index", id);
        }

    }
}
