﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProductController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private readonly VMResponse respons = new VMResponse();
        private int IdUser = 1;

        public apiProductController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblProduct> GetAllData()
        {
            List<VMTblProduct> data = ( from p in db.TblProducts
                                        join v in db.TblVariants on p.IdVariant equals v.Id
                                        join c in db.TblCategories on v.IdCategory equals c.Id
                                        where p.IsDelete == false
                                        select new VMTblProduct
                                        {
                                            Id = p.Id,
                                            NameProduct = p.NameProduct,
                                            Price = p.Price,
                                            Stock = p.Stock,
                                            Image = p.Image,

                                            IdVariant = p.IdVariant,
                                            NameVariant = v.NameVariant,

                                            IdCategory = v.IdCategory,
                                            NameCategory = c.NameCategory
                                        }
                                      ).ToList();

            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblProduct GetDataById(int id)
        {
            VMTblProduct data = (
                                    from p in db.TblProducts
                                    join v in db.TblVariants on p.IdVariant equals v.Id
                                    join c in db.TblCategories on v.IdCategory equals c.Id
                                    where p.IsDelete == false && p.Id == id
                                    select new VMTblProduct
                                    {
                                        Id = p.Id,
                                        NameProduct = p.NameProduct,
                                        NameVariant = v.NameVariant,
                                        NameCategory = c.NameCategory,
                                        Price = p.Price,
                                        Stock = p.Stock,
                                        Image = p.Image,
                                        CreateBy = p.CreateBy,
                                        CreateDate = p.CreateDate,
                                        UpdateBy = p.UpdateBy,
                                        UpdateDate = p.UpdateDate
                                    }
                                ).FirstOrDefault()!;

            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblProduct data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respons.Message = "Data success saved";
            }
            catch (Exception e)
            {
                respons.Success = false;
                respons.Message = "Failed saved " + e.Message;
            }
            return respons;
            }

        [HttpPut("Edit")]
        public VMResponse Edit(TblProduct data)
        {
            TblProduct dt = db.TblProducts.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.NameProduct = data.NameProduct;
                dt.Price = data.Price;
                dt.IdVariant = data.IdVariant;
                if(data.Image != null)
                {
                    dt.Image = data.Image;
                }
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respons.Message = "Data success updates";
                }
                catch (Exception e)
                {
                    respons.Success = false;
                    respons.Message = "Update failed " + e.Message;
                }
            }
            else
            {
                respons.Success = false;
                respons.Message = "Data not found";
            }
            return respons;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblProduct dt = db.TblProducts.Where(a => a.Id == id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respons.Message = $"Data {dt.NameProduct} success deleted";
                }
                catch (Exception e)
                {
                    respons.Success = false;
                    respons.Message = "Delete Failed " + e.Message;
                }
            }
            else
            {
                respons.Success = false;
                respons.Message = "Data not found";
            }
            return respons;
        }

    }
}
