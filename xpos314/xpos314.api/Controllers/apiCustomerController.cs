﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private readonly VMResponse respons = new VMResponse();
        private int IdUser = 1;

        public apiCustomerController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data = (from c in db.TblCustomers
                                       join r in db.TblRoles on c.IdRole equals r.Id
                                       where c.IsDelete == false
                                       select new VMTblCustomer
                                       {
                                           Id = c.Id,
                                           NameCustomer = c.NameCustomer,
                                           Email = c.Email,
                                           Address = c.Address,
                                           Phone = c.Phone,
                                           CreateDate = c.CreateDate,

                                           IdRole = r.Id,
                                           RoleName = r.RoleName
                                       }).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblCustomer GetDataById(int id)
        {
            VMTblCustomer data = (from c in db.TblCustomers
                                        join r in db.TblRoles on c.IdRole equals r.Id
                                        where c.IsDelete == false && c.Id == id
                                        select new VMTblCustomer
                                        {
                                            Id = c.Id,
                                            NameCustomer = c.NameCustomer,
                                            Email = c.Email,
                                            Address = c.Address,
                                            Phone = c.Phone,
                                            Password = c.Password,
                                            CreateBy = c.CreateBy,
                                            CreateDate = c.CreateDate,
                                            UpdateBy = c.UpdateBy,
                                            UpdateDate = c.UpdateDate,

                                            IdRole = r.Id,
                                            RoleName = r.RoleName
                                        }).FirstOrDefault()!;
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCustomer data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respons.Message = "Data success saved";
            }
            catch (Exception e)
            {
                respons.Success = false;
                respons.Message = "Failed saved " + e.Message;
            }
            return respons;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCustomer data)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.NameCustomer = data.NameCustomer;
                dt.Email = data.Email;
                dt.Address = data.Address;
                dt.Phone = data.Phone;
                dt.Password = data.Password;
                dt.IdRole = data.IdRole;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respons.Message = "Data success updates";
                }
                catch (Exception e)
                {
                    respons.Success = false;
                    respons.Message = "Update failed " + e.Message;
                }
            }
            else
            {
                respons.Success = false;
                respons.Message = "Data not found";
            }
            return respons;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respons.Message = $"Data {dt.NameCustomer} success deleted";
                }
                catch (Exception e)
                {
                    respons.Success = false;
                    respons.Message = "Delete Failed " + e.Message;
                }
            }
            else
            {
                respons.Success = false;
                respons.Message = "Data not found";
            }
            return respons;
        }

    }
}
