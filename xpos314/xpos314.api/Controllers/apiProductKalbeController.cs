﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProductKalbeController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private readonly VMResponse respon = new VMResponse();

        public apiProductKalbeController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<Product> GetAllData()
        {
            List<Product> data = db.Products.ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public Product GetDataById(int id)
        {
            Product result = new Product();

            result = db.Products.Where(a=>a.IntProductId == id).FirstOrDefault()!;
            return result;
        }

        [HttpPost("Save")]
        public VMResponse Save(Product data)
        {
            data.CreateDate = DateTime.Now;
            data.TxtProductCode = GenerateCode();

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data Success Saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved " + ex.Message;
            }

            return respon;
        }

        [HttpGet("GenerateCode")]
        public string GenerateCode()
        {
            string code = $"P";

            string digit = "";

            Product data = db.Products.OrderByDescending(a => a.TxtProductCode).FirstOrDefault()!;

            if (data != null)
            {
                string codeLast = data.TxtProductCode;
                string[] codeSplit = codeLast.Split("P");
                int intLast = int.Parse(codeSplit[2]) + 1;
                digit = intLast.ToString("0000");
            }
            else
            {
                digit = "0001";
            }

            return code + digit;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(Product data)
        {
            Product dt = db.Products.Where(a => a.IntProductId == data.IntProductId).FirstOrDefault()!;

            if (dt != null)
            {
                dt.TxtProdctName = data.TxtProdctName;
                dt.IntQuantity = data.IntQuantity;
                dt.DecPrice = data.DecPrice;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respon.Message = "Data Success Saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Saved Failed " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            Product dt = db.Products.Where(a => a.IntProductId == id).FirstOrDefault()!;

            if (dt != null)
            { 

                try
                {
                    db.Remove(dt);
                    db.SaveChanges();

                    respon.Message = $" Data {dt.TxtProdctName} Success Deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }
    }
}
