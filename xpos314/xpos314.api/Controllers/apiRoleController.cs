﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiRoleController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private readonly VMResponse respons = new VMResponse();
        private int IdUser = 1;

        public apiRoleController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblRole> GetAllData()
        {
            List<TblRole> data = db.TblRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblRole DataById(int id)
        {
            TblRole result = new TblRole();

            result = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpGet("CheckRoleByName/{name}")]
        public bool CheckName(string name)
        {
            TblRole data = db.TblRoles.Where(a => a.RoleName == name).FirstOrDefault()!;
            if (data != null)
            {
                return true;
            }
            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblRole data)
        {
            data.CreatedBy = IdUser;
            data.CreatedDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respons.Message = "Data Success Saved";
            }
            catch (Exception ex)
            {
                respons.Success = false;
                respons.Message = "Failed Saved " + ex.Message;
            }
            return respons;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblRole data)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.RoleName = data.RoleName;
                dt.UpdatedBy = IdUser;
                dt.UpdatedDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respons.Message = "Data Success Saved";
                }
                catch (Exception ex)
                {
                    respons.Success = false;
                    respons.Message = "Saved Failed " + ex.Message;
                }
            }
            else
            {
                respons.Success = false;
                respons.Message = "Data Not Found";
            }

            return respons;
        }

        [HttpDelete("Delete/{id}/{createdBy}")]
        public VMResponse Delete(int id, int createdBy)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.CreatedBy = createdBy;
                dt.UpdatedDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respons.Message = $" Data {dt.RoleName} Success Deleted";
                }
                catch (Exception ex)
                {
                    respons.Success = false;
                    respons.Message = "Delete Failed " + ex.Message;
                }
            }
            else
            {
                respons.Success = false;
                respons.Message = "Data Not Found";
            }
            return respons;
        }

    }
}
