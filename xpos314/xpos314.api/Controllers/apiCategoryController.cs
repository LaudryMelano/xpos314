﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCategoryController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private readonly VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCategoryController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblCategory> GetAllData()
        {
            List<TblCategory> data = db.TblCategories.Where(a=>a.IsDelete==false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblCategory DataById(int id)
        {
            TblCategory result = new TblCategory();

            result = db.TblCategories.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpGet("CheckCategoryByName/{name}")]
        public bool CheckName(string name)
        {
            TblCategory data = db.TblCategories.Where(a => a.NameCategory == name).FirstOrDefault()!;
            if (data != null)
            {
                return true;
            }
            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCategory data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data Success Saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCategory data) 
        {
            TblCategory dt = db.TblCategories.Where(a=>a.Id  == data.Id).FirstOrDefault()!;   

            if (dt != null)
            {
                dt.NameCategory = data.NameCategory;
                dt.Description = data.Description;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt); 
                    db.SaveChanges();
                    respon.Message = "Data Success Saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Saved Failed " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}/{createdBy}")]
        public VMResponse Delete(int id, int createdBy)
        {
            TblCategory dt = db.TblCategories.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.CreateBy = createdBy;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = $" Data {dt.NameCategory} Success Deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

    }

}
