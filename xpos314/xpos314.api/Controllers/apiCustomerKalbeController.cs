﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerKalbeController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private readonly VMResponse respon = new VMResponse();

        public apiCustomerKalbeController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<Customer> GetAllData()
        {
            List<Customer> data = db.Customers.ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public Customer GetDataById(int id)
        {
            Customer result = new Customer();

            result = db.Customers.Where(a => a.IntCustomerId == id).FirstOrDefault()!;
            return result;
        }

        [HttpPost("Save")]
        public VMResponse Save(Customer data)
        {
            data.Inserted = DateTime.Now;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data Success Saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(Customer data)
        {
            Customer dt = db.Customers.Where(a => a.IntCustomerId == data.IntCustomerId).FirstOrDefault()!;

            if (dt != null)
            {
                dt.TxtCustomerName = data.TxtCustomerName;
                dt.TxtCustomerAdrress = data.TxtCustomerAdrress;
                dt.BitGender = data.BitGender;
                dt.DtmBirthDate = data.DtmBirthDate;
                dt.Inserted = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respon.Message = "Data Success Saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Saved Failed " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            Customer dt = db.Customers.Where(a => a.IntCustomerId == id).FirstOrDefault()!;

            if (dt != null)
            {

                try
                {
                    db.Remove(dt);
                    db.SaveChanges();

                    respon.Message = $" Data {dt.TxtCustomerName} Success Deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }
    }
}
