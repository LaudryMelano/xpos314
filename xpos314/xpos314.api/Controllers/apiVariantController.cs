﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiVariantController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private readonly VMResponse respons = new VMResponse();
        private int IdUser = 1;

        public apiVariantController(XPOS_314Context _db)
        {
            this.db = _db;
        }


        [HttpGet("GetAllData")]
        public List<VMTblVariant> GetAllData()
        {
            List<VMTblVariant> data = (from v in db.TblVariants
                                       join c in db.TblCategories on v.IdCategory equals c.Id
                                       //where v.IsDelete == false
                                       select new VMTblVariant
                                       {
                                           Id = v.Id,
                                           NameVariant = v.NameVariant,
                                           Description = v.Description,
                                           IsDelete = v.IsDelete,
                                           CreateDate = v.CreateDate,

                                           IdCategory = v.IdCategory,
                                           NameCategory = c.NameCategory
                                       }).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblVariant GetDataById(int id)
        {
            VMTblVariant data = (from v in db.TblVariants
                                 join c in db.TblCategories on v.IdCategory equals c.Id
                                 where v.IsDelete == false && v.Id == id
                                 select new VMTblVariant
                                 {
                                     Id = v.Id,
                                     NameVariant = v.NameVariant,
                                     Description = v.Description,
                                     CreateBy = v.CreateBy,
                                     CreateDate = v.CreateDate,
                                     UpdateBy = v.UpdateBy,
                                     UpdateDate = v.UpdateDate,
                                     IdCategory = v.IdCategory,
                                     NameCategory = c.NameCategory
                                 }
                                     ).FirstOrDefault()!;
            return data;
        }

        [HttpGet("GetDataByIdCategory/{id}")]
        public List<VMTblVariant> GetDataByIdCategory(int id)
        {
            List<VMTblVariant> data = (from v in db.TblVariants
                                 join c in db.TblCategories on v.IdCategory equals c.Id
                                 where v.IsDelete == false && v.IdCategory == id
                                 select new VMTblVariant
                                 {
                                     Id = v.Id,
                                     NameVariant = v.NameVariant,
                                     Description = v.Description,
                                     CreateBy = v.CreateBy,
                                     CreateDate = v.CreateDate,
                                     UpdateBy = v.UpdateBy,
                                     UpdateDate = v.UpdateDate,
                                     IdCategory = v.IdCategory,
                                     NameCategory = c.NameCategory
                                 }).ToList()!;
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblVariant data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respons.Message = "Data success saved";
            }
            catch (Exception e)
            {
                respons.Success = false;
                respons.Message = "Failed saved " + e.Message;
            }
            return respons;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblVariant data)
        {
            TblVariant dt = db.TblVariants.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.NameVariant = data.NameVariant;
                dt.Description = data.Description;
                dt.IdCategory = data.IdCategory;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respons.Message = "Data success updates";
                }
                catch (Exception e)
                {
                    respons.Success = false;
                    respons.Message = "Update failed " + e.Message;
                }
            }
            else
            {
                respons.Success = false;
                respons.Message = "Data not found";
            }
            return respons;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblVariant dt = db.TblVariants.Where(a => a.Id == id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respons.Message = $"Data {dt.NameVariant} success deleted";
                }
                catch (Exception e)
                {
                    respons.Success = false;
                    respons.Message = "Delete Failed " + e.Message;
                }
            }
            else
            {
                respons.Success = false;
                respons.Message = "Data not found";
            }
            return respons;
        }

    }
}
