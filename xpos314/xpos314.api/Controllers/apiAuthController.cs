﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiAuthController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private readonly VMResponse respons = new VMResponse();
        private int IdUser = 1;

        public apiAuthController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("CheckLogin/{email}/{password}")]
        public VMTblCustomer CheckLogin(string email, string password)
        {
            VMTblCustomer data = (
                                    from c in db.TblCustomers
                                    join r in db.TblRoles on c.IdRole equals r.Id
                                    where c.IsDelete == false && c.Email == email && c.Password == password
                                    select new VMTblCustomer
                                    {
                                        Id = c.Id,
                                        NameCustomer = c.NameCustomer,
                                        IdRole = c.IdRole,
                                        RoleName = r.RoleName
                                    }
                                 ).FirstOrDefault()!;
            return data;
        }

        [HttpGet("MenuAccess/{IdRole}")]
        public List<VMMenuAccess> MenuAccess(int IdRole)
        {
            List<VMMenuAccess> listMenu = new List<VMMenuAccess>();

            listMenu = (from parent in db.TblMenus
                        join ma in db.TblMenuAccesses
                        on parent.Id equals ma.MenuId
                        where parent.IsParent == true && ma.IdRole == IdRole
                        select new VMMenuAccess
                        {
                            Id = parent.Id,
                            MenuName = parent.MenuName,
                            MenuIcon = parent.MenuIcon,
                            IdRole = ma.IdRole,
                            MenuSorting = parent.MenuSorting,
                            MenuAction = parent.MenuAction,
                            MenuController = parent.MenuController,
                            ListChild = (from child in db.TblMenus
                                         where child.MenuParent == parent.Id
                                         select new VMMenuAccess
                                         {
                                             Id = child.Id,
                                             MenuName = child.MenuName,
                                             MenuAction = child.MenuAction,
                                             MenuController = child.MenuController,
                                             MenuIcon = child.MenuIcon,
                                             MenuParent = child.MenuParent,
                                             MenuSorting = child.MenuSorting
                                         }).OrderBy(a => a.MenuSorting).ToList()
                        }).OrderBy(a => a.MenuSorting).ToList();

            return listMenu;
        }

        [HttpPost("CreateCustomer")]
        public VMResponse CreateCustomer(TblCustomer data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respons.Message = $"Register Success welcome, {data.NameCustomer}";
            }
            catch (Exception ex)
            {
                respons.Success = false;
                respons.Message = "Register Failed : " + ex.Message;
            }

            return respons;
        }

        [HttpGet("CheckEmailExisting/{email}")]
        public bool CheckEmailExisting(string email)
        {
            TblCustomer customer = db.TblCustomers.Where(a=> a.Email == email).FirstOrDefault()!;
            if (customer != null) 
            {
                return true;
            }
            return false;
        }

    }
}
