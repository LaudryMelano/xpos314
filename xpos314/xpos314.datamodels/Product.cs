﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("Product")]
    public partial class Product
    {
        [Key]
        [Column("intProductID")]
        public int IntProductId { get; set; }
        [Column("txtProductCode")]
        [StringLength(5)]
        [Unicode(false)]
        public string TxtProductCode { get; set; } = null!;
        [Column("txtProdctName")]
        [StringLength(100)]
        [Unicode(false)]
        public string TxtProdctName { get; set; } = null!;
        [Column("intQuantity")]
        public int IntQuantity { get; set; }
        [Column("decPrice", TypeName = "decimal(18, 2)")]
        public decimal DecPrice { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
    }
}
