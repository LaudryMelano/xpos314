﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("Penjualan")]
    public partial class Penjualan
    {
        [Key]
        [Column("intSalesOrderID")]
        public int IntSalesOrderId { get; set; }
        [Column("intCustomerID")]
        public int? IntCustomerId { get; set; }
        [Column("intProductID")]
        public int? IntProductId { get; set; }
        [Column("dtSalesOrder", TypeName = "datetime")]
        public DateTime? DtSalesOrder { get; set; }
        [Column("intQty")]
        public int? IntQty { get; set; }
    }
}
