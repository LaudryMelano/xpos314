﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("Customer")]
    public partial class Customer
    {
        [Key]
        [Column("intCustomerID")]
        public int IntCustomerId { get; set; }
        [Column("txtCustomerName")]
        [StringLength(100)]
        [Unicode(false)]
        public string TxtCustomerName { get; set; } = null!;
        [Column("txtCustomerAdrress")]
        [Unicode(false)]
        public string TxtCustomerAdrress { get; set; } = null!;
        [Column("bitGender")]
        public bool BitGender { get; set; }
        [Column("dtmBirthDate", TypeName = "datetime")]
        public DateTime DtmBirthDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Inserted { get; set; }
    }
}
